%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2013 koeart, john, ben                                         %
%                                                                          %
% This program is free software: you can redistribute it and/or modify it  %
% under the terms of the GNU General Public License as published by the    %
% Free Software Foundation, either version 3 of the License, or (at your   %
% option) any later version.                                               %
%                                                                          %
% This program is distributed in the hope that it will be useful, but      %
% WITHOUT ANY WARRANTY; without even the implied warranty of               %
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        %
% General Public License for more details.                                 %
%                                                                          %
% You should have received a copy of the GNU General Public License along  %
% with this program.  If not, see <http://www.gnu.org/licenses/>.          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ProvidesClass{datenschleuder}[%
	2013/07/19 v0.1 Datenschleuder
]
\LoadClass[%
	pdftex,
	a5paper,
	twoside,
%	twocolumn,
	fontsize=8pt,
]{scrartcl}

\IfFileExists{luatex85.sty}{%
	\RequirePackage{luatex85} % Fixes Problems with pdfxxx names [http://tex-live.tug.narkive.com/gc2DAmsJ/fwd-proble    m-with-lualatex-in-lexlive-2016]
}{}

\RequirePackage{textcomp}
\RequirePackage{fixltx2e}
\AtBeginDocument{\RequirePackage{ellipsis}}
\RequirePackage{microtype}
\RequirePackage[paper=a5paper,inner=16mm, outer=12mm,top=20mm,bottom=30mm]{geometry}


%FIXME: do so only if specified in document or makefile:
%\RequirePackage[cam,a4,center]{crop}
 

\RequirePackage{multicol}
\RequirePackage{xkeyval}
\newcommand{\@volume}{}
\DeclareOptionX{volume}{\renewcommand{\@volume}{#1}}%
\newcommand{\@year}{}
\DeclareOptionX{year}{\renewcommand{\@year}{#1}}%
\newif\iffree\freetrue
\DeclareOptionX{non-free}{\freefalse}%
\ProcessOptionsX

\RequirePackage{ifluatex}
\ifluatex
	\RequirePackage{luatextra}
	\RequirePackage{lineno}
	\RequirePackage[EU2]{fontenc}
	\RequirePackage{fontspec}
	\iffree
		\newcommand{\DSLogoFont}{\fontspec[Scale=2.36]{Zepto}}
		\setmainfont[Ligatures=TeX,Numbers={OldStyle}]{Linux Libertine O}
		\setsansfont[Ligatures=TeX,Numbers={OldStyle}]{Linux Biolinum O}
		%\setmonofont[Mapping=tex-text]{Courier Prime}
	\else
		\setmainfont[Ligatures=TeX,Numbers={OldStyle}]{FFScala}
		\setsansfont[Ligatures=TeX,Numbers={OldStyle}]{ScalaSans}
		\newcommand{\DSLogoFont}{\fontspec[Scale=2.93]{FFNetwork}}
	\fi
\else
	\ClassError{Datenschleuder}{%
		You need LuaTeX in order to compile \MessageBreak
  a Datenschleuder%
	}{%
		Instead of running "pdflatex \jobname" type:\MessageBreak
		lualatex \jobname\MessageBreak
		\MessageBreak
		Remember to cross two fingers and praise Eris before compiling.\MessageBreak
		Unpredictable things will happen with your computer otherwise...
	}
\fi

\RequirePackage{fmtcount}

\RequirePackage{listings}
\lstset{%
	upquote=true,
	basicstyle=\ttfamily,
	%backgroundcolor=\color{lightgray},
	%frame=single,
	stringstyle=\color{brown},
	keywordstyle=\color{yellow},
%  identifierstyle=\color{green},
	commentstyle=\color{gray},
	keywordstyle=[2]\color{darkgray},
	keywordstyle=[3]\color{darkgray},
	tabsize=2
}

\RequirePackage{booktabs}
\RequirePackage{colortbl}
\RequirePackage{scrlayer-scrpage}
\RequirePackage{graphicx}
\RequirePackage{tikz}
\RequirePackage{qrcode}
\RequirePackage[hyphens]{url}
\RequirePackage[pdftex,%
	citebordercolor={0.9 0.9 1},%
	filebordercolor={0.9 0.9 1},%
	linkbordercolor={0.9 0.9 1},%
	urlbordercolor={0.9 0.9 1},%
	pdfborder={0.9 0.9 1},%
	pagebackref,plainpages=false,pdfpagelabels=true]{hyperref}
\hypersetup{
	pdfauthor={Chaos Computer Club},
	pdftitle={datenschleuder},
	pdfsubject={wissenschaftliches fackblatt fuer datenreisende},
	pdfkeywords={Ausgabe\@volume, Jahr\@year},
	breaklinks,
	colorlinks,
	linkcolor = black,
	filecolor = black,
	urlcolor  = black,
	citecolor = black,
}
% Disable single lines at the start of a paragraph (Schusterjungen) {{{
\clubpenalty = 10000
% Disable single lines at the end of a paragraph (Hurenkinder)
\widowpenalty = 10000
\displaywidowpenalty = 10000
%}}}

\RequirePackage{wallpaper}
\RequirePackage{float}

\pgfdeclareimage[height=5mm]{Pesthoernchen}{schleuderpackung/img/pesthoernchen}
\newcommand{\chaosknoten}[1]{%
	\tikz[yscale=-#1,xscale=#1,every path/.style={fill=\DSPage@fg}]{\input{schleuderpackung/img/chaosknoten.tex}}%
}

\newfloat{impressum}{tpbh}{loi} %impressum als Float, damit kann Text fließen

\pagestyle{scrheadings}

\clearscrheadfoot
\setkomafont{pageheadfoot}{%
\normalfont\normalcolor\small
}
%FIXME: use pgfdeclared image for saving objects in pdf

\newcommand{\@qrpagecode}{\tikz[baseline=(foo.south)]{\node(foo)[inner sep=0]{\qrcode[height=.4cm,level=H]{\arabic{page}}}}}%

\rehead[]{\tikz[overlay]{\node[xshift=0.3mm,yshift=1.1mm]{\chaosknoten{0.017}};}}
\rohead[]{\tikz[overlay]{\node[xshift=0.3mm,yshift=1.1mm]{\chaosknoten{0.017}};}}
\chead{\textcolor{\DSPage@fg}{\directlua{tex.print(ruhetmp or "Bitte runninghead setzen")}}}
\setheadsepline{0.5pt}[\color{\DSPage@fg}]

\ifoot[]{\textcolor{\DSPage@fg}{\vphantom{\@qrpagecode}Datenschleuder. \@volume\ / \@year}}
\rofoot[]{\textcolor{\DSPage@fg}{\texttt{0x\ifthenelse{\value{page}>15}{}{0}\hexadecimal{page}} \@qrpagecode%
}}
\lefoot[]{\textcolor{\DSPage@fg}{\@qrpagecode\ \texttt{0x\ifthenelse{\value{page}>15}{}{0}\hexadecimal{page}}}}
\setfootsepline{0.5pt}[\color{\DSPage@fg}]

%FIXME: use abstract environment in prior of nasty DSabstract command
\renewenvironment{abstract}{%
	\begin{center}
		\bfseries
}{%
	\end{center}
}
\newcommand{\@DSabstract}{}
\newcommand{\DSabstract}[1]{\renewcommand{\@DSabstract}{#1}}

\newtoks{\@DSauthorstok}
\@DSauthorstok={}%
\newcommand{\@addauthor}[1]{\@DSauthorstok=\expandafter{\the\@DSauthorstok #1 }}
\newcommand{\addauthor}[1]{\@addauthor{#1}}


\renewcommand{\maketitle}{%
%	\twocolumn[
	%	\renewcommand{\@runninghead}{\@runningheadtmp}
	\hypertarget{AR\directlua{tex.print(nxruhetmp)}}{\textsf{\Huge\@title}}\pdfbookmark[0]{\directlua{tex.print(nxruhetmp)}}{AR\directlua{tex.print(nxruhetmp)}}
		\begin{center}%
			\textsf{von \@author}%FIXME: implement mail and array of authors
		\end{center}
        \addauthor{Noch ein Test}

		{\bfseries\@DSabstract}
		\vspace*{3mm}
%	]
		\directlua{ruhetmp=nxruhetmp nxruhetmp=nil}
	\normalsize
}

\define@key{DSarticleKeys}{title}{\title{#1}}%
\define@key{DSarticleKeys}{author}{\author{#1}}
\define@key{DSarticleKeys}{DSabstract}{\renewcommand{\@DSabstract}{#1}}
%\newcommand{\@runninghead}{}
%\newcommand{\@runningheadtmp}{}
%\newcommand{\runninghead}[1]{\renewcommand{\@runningheadtmp}{#1}}
\newcommand{\runninghead}[1]{\directlua{nxruhetmp = "\luatexluaescapestring{#1}"}}
\newcommand{\manrunninghead}[1]{\directlua{ruhetmp = "\luatexluaescapestring{#1}"}}
\define@key{DSarticleKeys}{runninghead}[\@title]{\runninghead{#1}}

\newenvironment{DSarticle}[1][]{%
    \newpage
    \setcounter{section}{0}
	\setkeys{DSarticleKeys}{#1}%
    \newpage
    \noindent\maketitle
    \begin{multicols}{2}
    }{%
    \end{multicols}
}
\newenvironment{DSfigure}
  {\par\medskip\noindent\minipage{\linewidth}
  }{%
  \endminipage\par\medskip}


\newcounter{NumOfLetters}

\newenvironment{DSletters}{%
	\hypertarget{MARKLeserbriefe}{}
	\pdfbookmark[0]{Leserbriefe}{MARKLesrbriefe}	
\manrunninghead{Leserbriefe}
\setcounter{NumOfLetters}{0}
\begin{multicols}{2}
}
{%
\end{multicols}
}

\newenvironment{letter}{%
	\setlength{\parskip}{2.5mm}%
	\setlength{\parindent}{0mm}%
	\ifthenelse{\equal{\arabic{NumOfLetters}}{0}}{}{%
		\vspace{-3mm}%
		\begin{center}%
			\hfill%
			\hfill%
%FIXME: use pgfdeclared image for saving objects in pdf
			\pgfuseimage{Pesthoernchen}
			\hfill%
			\pgfuseimage{Pesthoernchen}
			\hfill%
			\pgfuseimage{Pesthoernchen}
			\hfill%
			\hfill\hspace*{0cm}%
		\end{center}%
		\vspace{-5mm}%
	}%
	\stepcounter{NumOfLetters}%
}{%
}

\newcommand{\@letterauthor}{}
\define@key{DSletterKeys}{author}{\renewcommand{\@letterauthor}{#1}}%
\newenvironment{question}[1][]{%
	\setkeys{DSletterKeys}{#1}%
}{%
	\ifthenelse{%
		\equal{\@letterauthor}{}
		}{%
		}{%
			\textit{<\@letterauthor>}%
		}
\par 
}
\newenvironment{answer}[1][]{%
	\setkeys{DSletterKeys}{#1}%
		\itshape%
}{%
	\ifthenelse{%
		\equal{\@letterauthor}{}
		}{%
		}{%
			\textit{<\@letterauthor>}%
		}
}

%%%%%%%%
% Die folgenden Kommandos können auch aus dem Dokument gesetzt werden. Evtl. als DSimpressum Keys machen
%%%%%%%%
\newcommand{\@DSherausgeber}{}
\newcommand{\DSherausgeber}[1]{\renewcommand{\@DSherausgeber}{#1}}

\newcommand{\@DSredaktion}{}
\newcommand{\DSredaktion}[1]{\renewcommand{\@DSredaktion}{#1}}


\newcommand{\@DSdruck}{}
\newcommand{\DSdruck}[1]{\renewcommand{\@DSdruck}{#1}}


\newcommand{\@DSvisdp}{}
\newcommand{\DSvisdp}[1]{\renewcommand{\@DSvisdp}{#1}}

\newcommand{\@DSauthors}{\textbf{Autoren }\\\the\@DSauthorstok}
\newcommand{\DSauthors}{\@DSauthors}

%\define@key{DSimpressumKeys}{ViSdP}{%
%    \DSvisdp{#1}
%}
\newcommand{\DSimpressum}{%
    \begin{impressum*}[b!]
            %minipage: pos., höhe, textpos, breite
        \colorbox{lightgray}{
        \begin{minipage}[t]{1\linewidth}
         \hypertarget{MARKImpressum}{ \large \textbf{Die Datenschleuder Nr. \@volume}}
	\pdfbookmark[0]{Impressum}{MARKImpressum}	
                    \begin{multicols}{2}
                   \normalsize
                   \@DSherausgeber\\
 \@DSredaktion\\
 \@DSdruck\\
 \@DSvisdp\\
                    \DSauthors
                \end{multicols}
            \end{minipage}
        }
        \end{impressum*}
}

\RequirePackage{pgfplotstable}
\newcommand{\DSerfakreise}[1]{%
	\definecolor{color0}{gray}{0.9}%
	\definecolor{color1}{gray}{0.7}%
	\pgfplotstableread[col sep=semicolon]{#1}\addresstable%
	\pgfplotstablegetrowsof{\addresstable}%
	\pgfmathparse{\pgfplotsretval-1}%
	\foreach \index in {0,...,\pgfmathresult}{%
		\pgfmathparse{int(mod(\index,2))}%
		\definecolor{currentcolor}{named}{color\pgfmathresult}%
		\noindent\tikz[baseline,outer sep=0]{\node[
			text width=\textwidth,
			anchor=base,
			outer sep=0,
			%inner sep=0,
			fill=currentcolor
		]{%
		\pgfmathparse{int(mod(\index,2))}%
		\pgfplotstablegetelem{\index}{Erfa}\of\addresstable%
		\noindent\textbf{\pgfplotsretval}, %
		\pgfplotstablegetelem{\index}{Ort}\of\addresstable%
		\ifthenelse{\equal{\pgfplotsretval}{}}{}{\pgfplotsretval, }%
		\pgfplotstablegetelem{\index}{Tag}\of\addresstable%
		\pgfplotsretval, %
		\pgfplotstablegetelem{\index}{Adresse}\of\addresstable%
		\pgfplotsretval, %
		\pgfplotstablegetelem{\index}{Bemerkung}\of\addresstable%
		\url{\pgfplotsretval}%
		}}%
		\\[-1pt]
	}
}

\RequirePackage{environ}
\RequirePackage{eurosym}
\newcommand{\DSPage@fg}{black}
\define@key{DSTitlePage}{fg}[]{\renewcommand{\DSPage@fg}{#1}}
\newcommand{\DSTitlePage@bg}{white}
\define@key{DSTitlePage}{bg}[]{\renewcommand{\DSTitlePage@bg}{#1}}
\newcommand{\DSTitlePage@price}{2,50\,\officialeuro}
\define@key{DSTitlePage}{price}[]{\renewcommand{\DSTitlePage@price}{#1}}

\usetikzlibrary{positioning}

\newlength{\bodywidth}\setlength{\bodywidth}{129mm}
\newlength{\logoXsep}\setlength{\logoXsep}{3mm}

\NewEnviron{DSTitlePage}[1][]{%
\setkeys{DSTitlePage}{#1}%
\thispagestyle{empty}
\begin{tikzpicture}[
		remember picture,
		overlay,
		every node/.append style={color=\DSPage@fg},
		color=\DSPage@fg,
	]
	\path[fill=\DSTitlePage@bg] (current page.north west) rectangle (current page.south east);
	\node[
		font=\DSLogoFont\Huge,
		inner sep=\logoXsep,
		below=6.5mm of current page.north,
	](logo){%
		die datenschleuder.%
	};

	\draw[line width=4pt] (logo.south west) -- +(\bodywidth,0);
	\node[%
		below right=\logoXsep of logo.south west,
		text width=\columnwidth,
		font=\textbf,  %FIX for texlive 2016
		inner xsep=0mm,
	](DSsubtitle){%
		das wissenschaftliche fachblatt für datenreisende\\
		ein organ des chaos computer club
	};
	\node[
		above left=0cm of logo.east|-current page.south,
	](chaosknoten){%
		\chaosknoten{0.035}
	};
	\node[
		left=0cm of chaosknoten.north west,
		anchor=north east,
	]{\fontspec[Scale=3.9]{Linux Biolinum O}\#\@volume};
	\node[
		below right=0cm of logo.west|-chaosknoten.north,
		inner xsep=0pt,
		text width=\columnwidth,
		font=\textbf,
	] (issn) {%
		ISSN 0930-1054 \textbullet\ \@year\\
		\DSTitlePage@price
	};
	\draw[thick] ([yshift=1mm]issn.north west) -- ([yshift=1mm]chaosknoten.north east);
	\node[%
		below right=0mm of DSsubtitle.south-|logo.west,
		inner xsep=0pt,
		]{%
			\begin{minipage}{\bodywidth}
				\BODY
			\end{minipage}
		};
\end{tikzpicture}%
\normalfont
\clearpage
}

\define@key{DSFullImgPage}{fg}[]{\renewcommand{\DSPage@fg}{#1}}
\newcommand{\DSFullImgPage@running}{}
\define@key{DSFullImgPage}{runninghead}[]{\renewcommand{\DSFullImgPage@running}{#1}}
\define@key{DSFullImgPage}{picturepuzzle}[]{%
	\renewcommand{\DSFullImgPage@running}{Das große Datenschleuder-Leser-Bilder-Rätsel}%
}
\newcommand{\DSFullImgPage}[2][runninghead=]{%
	\setkeys{DSFullImgPage}{#1}
	\clearpage
	\manrunninghead{\DSFullImgPage@running}
	\ifthenelse{\equal{\DSFullImgPage@running}{}}{\pagestyle{plain}}{}
	\ThisCenterWallPaper{1}{#2}~
	\clearpage
	\renewcommand{\DSPage@fg}{black}
	\pagestyle{scrheadings}
}
