# Einleitung
Mit schleuderpackung existiert eine Latex-Klasse, welche es ermöglicht ein Heft wie die Datenschleuder zu erzeugen.

# Verwendung
Requirements:
Latex:
texlive-lualatex und viele andere Pakete


Setup, damit alle Komponenten von Schleuderpackung verwendet werden können.
Git Repo in einem Verzeichnis deiner Wahl auschecken (im Beispiel: schleuderpackung für eine neue Schleuder verwenden):

```
schleuder_repourl=git://git.c3d2.de/schleuderpackung.git
cd ~/neue-schleuder
schleuder_gitparent=$( pwd )
git clone $(schleuder_repourl) schleuderpackung
# fuer weitere Vorgaenge speichern
export schleuder_gitparent
# randnotiz:
# according to: http://tex.stackexchange.com/questions/10498/installing-a-class
# eigene cls nicht auf system sondern in lokalem verzeichnis erstellen

# Links auf datenschleuder.cls erstellen
ln -s ${schleuder_gitparent}/schleuderpackung
ln -s ${schleuder_gitparent}/schleuderpackung/datenschleuder.cls

# Logo herunterladen
cd ${schleuder_gitparent}/schleuderpackung/img
wget https://upload.wikimedia.org/wikipedia/en/d/d1/Logo_CCC.svg

# PDF aus den SVG bauen
make

# Freie Font [Zepto](http://www.dafont.com/zepto.font) herunterladen
wget http://dl.dafont.com/dl/?f=zepto -O /tmp/zepto.zip

# und nach `~/.fonts` entpacken
mkdir -p ~/.fonts
unzip /tmp/zepto.zip -d ~/.fonts

# Mit `font-manager` kann man checken, ob die Schrift vom System gefunden wird.
fc-list|grep -i zepto && echo "font found" || echo "font not found"
```

Nun kann man das Paket in seinen Latex Dokumenten benutzen.

