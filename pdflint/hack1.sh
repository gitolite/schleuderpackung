#!/bin/bash

#pdf2ps $1 - | ps2pdf13 - $2
pdf2ps $1 | gs -q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$2 -dCompatibilityLevel=1.3 -dPDFSETTINGS=/prepress -c .setpdfwrite -f -
